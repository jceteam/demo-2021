import { Component, OnInit, Input,  Output, EventEmitter } from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {
  //@Input() book: Book;
  @Input() name: string;
  @Input() years: number;
  @Input() id: string;
  @Input() income: number;
  @Input() formType: string;
  
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();

  buttonText:String = 'Add customer'; 
  
  onSubmit(){ 

  }  

  tellParentToClose(){
    this.closeEdit.emit();
  }
  
  updateParent(){
    let customer:Customer = {id:this.id,name:this.name, years:this.years,income:this.income}; 
    this.update.emit(customer);
    if(this.formType == 'Add Book'){
      this.name = null;
      this.years = null;
      this.income = null; 
    }
  }

  constructor() { }


  ngOnInit(): void {
    if(this.formType == 'Add Book'){
      this.buttonText = 'Add';
    }
  }

}
