import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private authService:AuthService,
    private router:Router) { }

email:string;
password:string; 
error:string = 'no error'; 
hasError:Boolean = false; 
onSubmit(){
  this.authService.SignUp(this.email,this.password)
  .then(res => 
    {
      console.log('Succesful sign up',res);
    }
  ).catch(
    err => {
      console.log('in the catch')
      this.hasError = true;
      this.error = err.message;  
      console.log(this.error);
    }
  );
}

  ngOnInit(): void {
  }

}
